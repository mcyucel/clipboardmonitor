﻿namespace ClipboardMonitor
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.rtbClipboard = new System.Windows.Forms.RichTextBox();
            this.chbMonitor = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.chbMonitorImage = new System.Windows.Forms.CheckBox();
            this.chbMonitorText = new System.Windows.Forms.CheckBox();
            this.grbTextAction = new System.Windows.Forms.GroupBox();
            this.pnlImage = new System.Windows.Forms.Panel();
            this.rdbImageSaveToFile = new System.Windows.Forms.RadioButton();
            this.rdbOpenImageInternal = new System.Windows.Forms.RadioButton();
            this.rdbTextToFile = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.rdbToTextBox = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.chbTopmost = new System.Windows.Forms.CheckBox();
            this.pnlRtb = new System.Windows.Forms.Panel();
            this.sfdText = new System.Windows.Forms.SaveFileDialog();
            this.pnlTextFile = new System.Windows.Forms.Panel();
            this.txtTextFilePath = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.pnlImageSettings = new System.Windows.Forms.Panel();
            this.txtImageFolder = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.fbd = new System.Windows.Forms.FolderBrowserDialog();
            this.label6 = new System.Windows.Forms.Label();
            this.chbSmall = new System.Windows.Forms.CheckBox();
            this.chbStopIfFocused = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.grbTextAction.SuspendLayout();
            this.pnlImage.SuspendLayout();
            this.pnlRtb.SuspendLayout();
            this.pnlTextFile.SuspendLayout();
            this.pnlImageSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // rtbClipboard
            // 
            this.rtbClipboard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbClipboard.Location = new System.Drawing.Point(0, 0);
            this.rtbClipboard.Name = "rtbClipboard";
            this.rtbClipboard.Size = new System.Drawing.Size(351, 277);
            this.rtbClipboard.TabIndex = 0;
            this.rtbClipboard.Text = "";
            // 
            // chbMonitor
            // 
            this.chbMonitor.AutoSize = true;
            this.chbMonitor.Location = new System.Drawing.Point(12, 12);
            this.chbMonitor.Name = "chbMonitor";
            this.chbMonitor.Size = new System.Drawing.Size(108, 17);
            this.chbMonitor.TabIndex = 1;
            this.chbMonitor.Text = "Monitor Clipboard";
            this.chbMonitor.UseVisualStyleBackColor = true;
            this.chbMonitor.CheckedChanged += new System.EventHandler(this.chbMonitor_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBox3);
            this.groupBox1.Controls.Add(this.chbMonitorImage);
            this.groupBox1.Controls.Add(this.chbMonitorText);
            this.groupBox1.Location = new System.Drawing.Point(12, 35);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(327, 47);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Monitor for:";
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Enabled = false;
            this.checkBox3.Location = new System.Drawing.Point(247, 19);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(53, 17);
            this.checkBox3.TabIndex = 2;
            this.checkBox3.Text = "Audio";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // chbMonitorImage
            // 
            this.chbMonitorImage.AutoSize = true;
            this.chbMonitorImage.Location = new System.Drawing.Point(123, 19);
            this.chbMonitorImage.Name = "chbMonitorImage";
            this.chbMonitorImage.Size = new System.Drawing.Size(55, 17);
            this.chbMonitorImage.TabIndex = 1;
            this.chbMonitorImage.Text = "Image";
            this.chbMonitorImage.UseVisualStyleBackColor = true;
            // 
            // chbMonitorText
            // 
            this.chbMonitorText.AutoSize = true;
            this.chbMonitorText.Checked = true;
            this.chbMonitorText.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbMonitorText.Location = new System.Drawing.Point(6, 19);
            this.chbMonitorText.Name = "chbMonitorText";
            this.chbMonitorText.Size = new System.Drawing.Size(47, 17);
            this.chbMonitorText.TabIndex = 0;
            this.chbMonitorText.Text = "Text";
            this.chbMonitorText.UseVisualStyleBackColor = true;
            // 
            // grbTextAction
            // 
            this.grbTextAction.Controls.Add(this.chbStopIfFocused);
            this.grbTextAction.Controls.Add(this.pnlImage);
            this.grbTextAction.Controls.Add(this.rdbTextToFile);
            this.grbTextAction.Controls.Add(this.label2);
            this.grbTextAction.Controls.Add(this.rdbToTextBox);
            this.grbTextAction.Controls.Add(this.label1);
            this.grbTextAction.Location = new System.Drawing.Point(12, 88);
            this.grbTextAction.Name = "grbTextAction";
            this.grbTextAction.Size = new System.Drawing.Size(327, 136);
            this.grbTextAction.TabIndex = 3;
            this.grbTextAction.TabStop = false;
            this.grbTextAction.Text = "Action";
            // 
            // pnlImage
            // 
            this.pnlImage.Controls.Add(this.rdbImageSaveToFile);
            this.pnlImage.Controls.Add(this.rdbOpenImageInternal);
            this.pnlImage.Location = new System.Drawing.Point(13, 91);
            this.pnlImage.Name = "pnlImage";
            this.pnlImage.Size = new System.Drawing.Size(310, 32);
            this.pnlImage.TabIndex = 4;
            // 
            // rdbImageSaveToFile
            // 
            this.rdbImageSaveToFile.AutoSize = true;
            this.rdbImageSaveToFile.Location = new System.Drawing.Point(155, 5);
            this.rdbImageSaveToFile.Name = "rdbImageSaveToFile";
            this.rdbImageSaveToFile.Size = new System.Drawing.Size(85, 17);
            this.rdbImageSaveToFile.TabIndex = 2;
            this.rdbImageSaveToFile.TabStop = true;
            this.rdbImageSaveToFile.Text = "Save To File";
            this.rdbImageSaveToFile.UseVisualStyleBackColor = true;
            // 
            // rdbOpenImageInternal
            // 
            this.rdbOpenImageInternal.AutoSize = true;
            this.rdbOpenImageInternal.Checked = true;
            this.rdbOpenImageInternal.Location = new System.Drawing.Point(3, 5);
            this.rdbOpenImageInternal.Name = "rdbOpenImageInternal";
            this.rdbOpenImageInternal.Size = new System.Drawing.Size(136, 17);
            this.rdbOpenImageInternal.TabIndex = 0;
            this.rdbOpenImageInternal.TabStop = true;
            this.rdbOpenImageInternal.Text = "Open In Internal Viewer";
            this.rdbOpenImageInternal.UseVisualStyleBackColor = true;
            // 
            // rdbTextToFile
            // 
            this.rdbTextToFile.AutoSize = true;
            this.rdbTextToFile.Location = new System.Drawing.Point(211, 32);
            this.rdbTextToFile.Name = "rdbTextToFile";
            this.rdbTextToFile.Size = new System.Drawing.Size(85, 17);
            this.rdbTextToFile.TabIndex = 3;
            this.rdbTextToFile.TabStop = true;
            this.rdbTextToFile.Text = "Save To File";
            this.rdbTextToFile.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Image";
            // 
            // rdbToTextBox
            // 
            this.rdbToTextBox.AutoSize = true;
            this.rdbToTextBox.Checked = true;
            this.rdbToTextBox.Location = new System.Drawing.Point(13, 32);
            this.rdbToTextBox.Name = "rdbToTextBox";
            this.rdbToTextBox.Size = new System.Drawing.Size(106, 17);
            this.rdbToTextBox.TabIndex = 1;
            this.rdbToTextBox.TabStop = true;
            this.rdbToTextBox.Text = "Copy To Textbox";
            this.rdbToTextBox.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Text";
            // 
            // chbTopmost
            // 
            this.chbTopmost.AutoSize = true;
            this.chbTopmost.Location = new System.Drawing.Point(135, 12);
            this.chbTopmost.Name = "chbTopmost";
            this.chbTopmost.Size = new System.Drawing.Size(98, 17);
            this.chbTopmost.TabIndex = 4;
            this.chbTopmost.Text = "Always On Top";
            this.chbTopmost.UseVisualStyleBackColor = true;
            this.chbTopmost.CheckedChanged += new System.EventHandler(this.chbTopmost_CheckedChanged);
            // 
            // pnlRtb
            // 
            this.pnlRtb.Controls.Add(this.rtbClipboard);
            this.pnlRtb.Location = new System.Drawing.Point(0, 453);
            this.pnlRtb.Name = "pnlRtb";
            this.pnlRtb.Size = new System.Drawing.Size(351, 277);
            this.pnlRtb.TabIndex = 5;
            // 
            // sfdText
            // 
            this.sfdText.CreatePrompt = true;
            this.sfdText.DefaultExt = "txt";
            this.sfdText.Filter = "Text Files|*.txt";
            // 
            // pnlTextFile
            // 
            this.pnlTextFile.Controls.Add(this.txtTextFilePath);
            this.pnlTextFile.Controls.Add(this.label4);
            this.pnlTextFile.Location = new System.Drawing.Point(0, 349);
            this.pnlTextFile.Name = "pnlTextFile";
            this.pnlTextFile.Size = new System.Drawing.Size(351, 98);
            this.pnlTextFile.TabIndex = 6;
            // 
            // txtTextFilePath
            // 
            this.txtTextFilePath.Location = new System.Drawing.Point(3, 25);
            this.txtTextFilePath.Multiline = true;
            this.txtTextFilePath.Name = "txtTextFilePath";
            this.txtTextFilePath.ReadOnly = true;
            this.txtTextFilePath.Size = new System.Drawing.Size(336, 53);
            this.txtTextFilePath.TabIndex = 1;
            this.txtTextFilePath.Click += new System.EventHandler(this.textBox1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Text File Path:";
            // 
            // pnlImageSettings
            // 
            this.pnlImageSettings.Controls.Add(this.txtImageFolder);
            this.pnlImageSettings.Controls.Add(this.label5);
            this.pnlImageSettings.Location = new System.Drawing.Point(0, 243);
            this.pnlImageSettings.Name = "pnlImageSettings";
            this.pnlImageSettings.Size = new System.Drawing.Size(351, 100);
            this.pnlImageSettings.TabIndex = 7;
            // 
            // txtImageFolder
            // 
            this.txtImageFolder.Location = new System.Drawing.Point(12, 29);
            this.txtImageFolder.Multiline = true;
            this.txtImageFolder.Name = "txtImageFolder";
            this.txtImageFolder.ReadOnly = true;
            this.txtImageFolder.Size = new System.Drawing.Size(327, 43);
            this.txtImageFolder.TabIndex = 1;
            this.txtImageFolder.Click += new System.EventHandler(this.txtImageFolder_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Image Folder:";
            // 
            // fbd
            // 
            this.fbd.RootFolder = System.Environment.SpecialFolder.DesktopDirectory;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label6.Location = new System.Drawing.Point(132, 227);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "SETTINGS";
            // 
            // chbSmall
            // 
            this.chbSmall.AutoSize = true;
            this.chbSmall.Location = new System.Drawing.Point(259, 12);
            this.chbSmall.Name = "chbSmall";
            this.chbSmall.Size = new System.Drawing.Size(75, 17);
            this.chbSmall.TabIndex = 9;
            this.chbSmall.Text = "Mini Mode";
            this.chbSmall.UseVisualStyleBackColor = true;
            this.chbSmall.CheckedChanged += new System.EventHandler(this.chbSmall_CheckedChanged);
            // 
            // chbStopIfFocused
            // 
            this.chbStopIfFocused.AutoSize = true;
            this.chbStopIfFocused.Checked = true;
            this.chbStopIfFocused.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbStopIfFocused.Location = new System.Drawing.Point(15, 55);
            this.chbStopIfFocused.Name = "chbStopIfFocused";
            this.chbStopIfFocused.Size = new System.Drawing.Size(190, 17);
            this.chbStopIfFocused.TabIndex = 5;
            this.chbStopIfFocused.Text = "Do Not Copy if Textbox Has Focus";
            this.chbStopIfFocused.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(351, 744);
            this.Controls.Add(this.chbSmall);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.pnlImageSettings);
            this.Controls.Add(this.pnlTextFile);
            this.Controls.Add(this.pnlRtb);
            this.Controls.Add(this.chbTopmost);
            this.Controls.Add(this.grbTextAction);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.chbMonitor);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Clipboard Monitor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grbTextAction.ResumeLayout(false);
            this.grbTextAction.PerformLayout();
            this.pnlImage.ResumeLayout(false);
            this.pnlImage.PerformLayout();
            this.pnlRtb.ResumeLayout(false);
            this.pnlTextFile.ResumeLayout(false);
            this.pnlTextFile.PerformLayout();
            this.pnlImageSettings.ResumeLayout(false);
            this.pnlImageSettings.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtbClipboard;
        private System.Windows.Forms.CheckBox chbMonitor;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox chbMonitorImage;
        private System.Windows.Forms.CheckBox chbMonitorText;
        private System.Windows.Forms.GroupBox grbTextAction;
        private System.Windows.Forms.RadioButton rdbTextToFile;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rdbToTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chbTopmost;
        private System.Windows.Forms.Panel pnlRtb;
        private System.Windows.Forms.SaveFileDialog sfdText;
        private System.Windows.Forms.Panel pnlTextFile;
        private System.Windows.Forms.TextBox txtTextFilePath;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel pnlImage;
        private System.Windows.Forms.RadioButton rdbImageSaveToFile;
        private System.Windows.Forms.RadioButton rdbOpenImageInternal;
        private System.Windows.Forms.Panel pnlImageSettings;
        private System.Windows.Forms.TextBox txtImageFolder;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.FolderBrowserDialog fbd;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox chbSmall;
        private System.Windows.Forms.CheckBox chbStopIfFocused;
    }
}

