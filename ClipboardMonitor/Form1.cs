﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;

namespace ClipboardMonitor
{
    public partial class Form1 : Form
    {

        [DllImport("User32.dll")]
        protected static extern int
                  SetClipboardViewer(int hWndNewViewer);
        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        public static extern bool
               ChangeClipboardChain(IntPtr hWndRemove,
                                    IntPtr hWndNewNext);
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int SendMessage(IntPtr hwnd, int wMsg,
                                             IntPtr wParam,
                                             IntPtr lParam);

        IntPtr nextClipboardViewer;
        StringBuilder sb;


        // event fires on first run
        bool firstRun;

        public Form1()
        {
            InitializeComponent();
            this.firstRun = true;
            this.sb = new StringBuilder();
            this.txtImageFolder.Text = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
            this.txtTextFilePath.Text = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Clipboard.txt");
        }

        protected override void WndProc(ref System.Windows.Forms.Message m)
        {
            try
            {
                // defined in winuser.h
                const int WM_DRAWCLIPBOARD = 0x308;
                const int WM_CHANGECBCHAIN = 0x030D;

                switch (m.Msg)
                {
                    case WM_DRAWCLIPBOARD:
                        CopyClipboardContent();
                        SendMessage(nextClipboardViewer, m.Msg, m.WParam,
                                    m.LParam);
                        break;
                    case WM_CHANGECBCHAIN:
                        if (m.WParam == nextClipboardViewer)
                            nextClipboardViewer = m.LParam;
                        else
                            SendMessage(nextClipboardViewer, m.Msg, m.WParam,
                                        m.LParam);
                        break;

                    default:
                        base.WndProc(ref m);
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,"Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }

        private void CopyClipboardContent()
        {
            if (!this.firstRun && Clipboard.ContainsText())
            {
                if (chbMonitorText.Checked)
                {
                    if (this.rdbToTextBox.Checked)
                    {
                        AppentToTextBox();
                    }
                    else
                    {
                        AppendToFile();
                    }
                }
            }
            else if (!this.firstRun && Clipboard.ContainsImage())
            {
                if (chbMonitorImage.Checked)
                {
                    if (this.rdbOpenImageInternal.Checked)
                    {
                        OpenImageInternal();
                    }
                    else
                    {
                        SaveImageToFile();
                    }  
                }
            }
        }

        private void SaveImageToFile()
        {
            string uniqueFileDate = DateTime.UtcNow.ToString("yyyy-MM-dd-HH-m-ss-fff", System.Globalization.CultureInfo.InvariantCulture);
            Clipboard.GetImage().Save(Path.Combine(txtImageFolder.Text, String.Format("{0}.png", uniqueFileDate)));
        }


        private void OpenImageInternal()
        {
            ImageViewer newViewer = new ImageViewer();
            newViewer.Show();
        }

        private void AppentToTextBox()
        {
            if (!(this.chbStopIfFocused.Checked && this.rtbClipboard.Focused))
            {
                this.sb.Append(this.rtbClipboard.Text);
                this.sb.AppendLine(Clipboard.GetText());
                this.rtbClipboard.Text = sb.ToString();
                this.sb.Clear(); 
            } 
        }

        private void AppendToFile()
        {
            throw new NotImplementedException("This feature is not implemented yet.");
        }

        private void chbMonitor_CheckedChanged(object sender, EventArgs e)
        {
            if ((sender as CheckBox).Checked)
            {
                this.nextClipboardViewer = (IntPtr)SetClipboardViewer((int)this.Handle);
                this.firstRun = false;
            }
            else
            {
                ChangeClipboardChain(this.Handle, this.nextClipboardViewer);
                this.firstRun = true;
            }
        }

        private void chbTopmost_CheckedChanged(object sender, EventArgs e)
        {
            this.TopMost = ((sender as CheckBox).Checked);
        }




        private void textBox1_Click(object sender, EventArgs e)
        {
            if (this.sfdText.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.txtTextFilePath.Text = this.sfdText.FileName;
            }
        }

        private void txtImageFolder_Click(object sender, EventArgs e)
        {
            fbd.SelectedPath = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
            if (this.fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.txtImageFolder.Text = this.fbd.SelectedPath;
            }
        }

        private void chbSmall_CheckedChanged(object sender, EventArgs e)
        {
            this.Height = (sender as CheckBox).Checked == true ? 76 : 716;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            ChangeClipboardChain(this.Handle, this.nextClipboardViewer);
        }
    }
}
