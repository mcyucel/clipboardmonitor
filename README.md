# README #

This app is for monitoring clipboard on Windows systems for text and images. The program can put the copied text into its own textbox for ease of use.
In addition, copied images can be opened immediately with internal image viewer (where they can be saved manually) or they can automatically be saved to a user defined folder.